package com.store.Samsung.Resource;

import com.store.Samsung.model.Device;
import com.store.Samsung.model.Devices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/samsung")
public class Samsung {
    @RequestMapping("/devices")
    public Devices getDevice()
    {

        List<Device> devices=new ArrayList<>();
        devices.add(new Device("Note 9","SmartPhone"));
        devices.add(new Device("S10","SmartPhone"));
        return (new Devices(devices));
    }
}
